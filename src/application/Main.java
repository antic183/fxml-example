package application;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;


public class Main extends Application {
	private Stage root;
	
	
	@Override
	public void start(Stage primaryStage) {
		root = primaryStage;
		initRootWindow();
	}
	
	private void initRootWindow(){
		try {
			FXMLLoader loader = new FXMLLoader(Main.class.getResource("/view/RootWindow.fxml"));
			AnchorPane rootPane = loader.load();
			
			MainWindowController mainController = loader.getController();
			mainController.setMain(this);

			Scene scene = new Scene(rootPane);
			root.setScene(scene);
			root.show();
		} catch (IOException e) {
			System.out.println("FEHLER!");
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}

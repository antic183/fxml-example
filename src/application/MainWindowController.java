package application;


import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class MainWindowController {

	@FXML private Main main;

	@FXML private Label label;
	@FXML private TextField textField;

	public void setMain(Main main) {
		this.main = main;
	}

	@FXML
	private void changeContent() {
		String newContent = textField.getText(); 

		label.setText(newContent);
		textField.clear();
	}
	

}
